Basic template for a node.js app that runs in a docker container

Remember that when starting a project using this template you need to:

* Clone the repository using the name for your new project

* Delete the .git folder

* Initialize a new git repository

* Replace the name, repository, and author properties in package.json